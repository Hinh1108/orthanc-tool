/**
 * Orthanc - A Lightweight, RESTful DICOM Store
 * Copyright (C) 2012-2016 Sebastien Jodogne, Medical Physics
 * Department, University Hospital of Liege, Belgium
 * Copyright (C) 2017-2018 Osimis S.A., Belgium
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 **/


// For IE compatibility
if (!window.console) window.console = {};
if (!window.console.log) window.console.log = function () { };

// http://stackoverflow.com/a/21903119/881731
function GetUrlParameter(sParam) {
  var sPageURL = decodeURIComponent(window.location.search.substring(1));
  var sURLVariables = sPageURL.split('&');
  var sParameterName;
  var i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined ? '' : sParameterName[1];
    }
  }

  return '';
};

function httpGet(theUrl)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false ); // false for synchronous request
    xmlHttp.send( null );
    return xmlHttp.responseText;
}

function addInteraction(type1) {
  console.log(type1);
}

$(document).ready(function () {
  var seriesId = GetUrlParameter('series');
  if (seriesId.length == 0) {
    alert('Error - No series ID specified!');
  }
  else {
    $.ajax({
      url: '../pyramids/' + seriesId,
      error: function () {
        alert('Error - Cannot get the pyramid structure of series: ' + seriesId);
      },
      success: function (series) {
        //console.log(series);
        var resp = httpGet("http://localhost/orthanc/index.php?uid=23423dfedfsdfd");
        
        var data = JSON.parse(resp);
        console.log(1234);
       
        var res = data.PixelSpacing.split("\\");
        console.log(res);
        var spacingRow = parseFloat(res[0]);
        var spacingCol = parseFloat(res[1]);
        /*var request = new XMLHttpRequest();

        request.open('GET', 'http://localhost/orthanc/index.php?uid=23423dfedfsdfd', true);
        request.onload = function () {
          console.log("sdfsdfs");
          // Begin accessing JSON data here
          var data = JSON.parse(this.response);
          console.log(data);
          if (request.status >= 200 && request.status < 400) {
            data.forEach(movie => {
              console.log(movie.title);
            });
          } else {
            console.log('error');
          }
        }
        
        request.send();*/

        var source = new ol.source.Vector();

        var vector = new ol.layer.Vector({
          source: source,
          style: new ol.style.Style({
            fill: new ol.style.Fill({
              color: 'rgba(255, 255, 255, 0.2)'
            }),
            stroke: new ol.style.Stroke({
              color: '#40ff32',
              width: 2
            }),
            image: new ol.style.Circle({
              radius: 7,
              fill: new ol.style.Fill({
                color: '#40ff32'
              })
            })
          })
        });


        /**
         * Currently drawn feature.
         * @type {module:ol.Feature~Feature}
         */
        var sketch;


        /**
         * The help tooltip element.
         * @type {Element}
         */
        var helpTooltipElement;


        /**
         * Overlay to show the help messages.
         * @type {module:ol.Overlay}
         */
        var helpTooltip;


        /**
         * The measure tooltip element.
         * @type {Element}
         */
        var measureTooltipElement;


        /**
         * Overlay to show the measurement.
         * @type {module:ol.Overlay}
         */
        var measureTooltip;


        /**
         * Message to show when the user is drawing a polygon.
         * @type {string}
         */
        var continuePolygonMsg = 'Click to continue drawing the polygon';


        /**
         * Message to show when the user is drawing a line.
         * @type {string}
         */
        var continueLineMsg = 'Click to continue drawing the line';


        /**
         * Handle pointer move.
         * @param {module:ol.MapBrowserEvent~MapBrowserEvent} evt The event.
         */
        var pointerMoveHandler = function (evt) {
          if (evt.dragging) {
            return;
          }
          /** @type {string} */
          var helpMsg = 'Click to start drawing';

          if (sketch) {
            var geom = (sketch.getGeometry());
            if (geom instanceof ol.geom.Polygon) {
              helpMsg = continuePolygonMsg;
            } else if (geom instanceof ol.geom.LineString) {
              helpMsg = continueLineMsg;
            }
          }

          helpTooltipElement.innerHTML = helpMsg;
          helpTooltip.setPosition(evt.coordinate);

          helpTooltipElement.classList.remove('hidden');
        };


        var width = series['TotalWidth'];
        var height = series['TotalHeight'];
        var tileWidth = series['TileWidth'];
        var tileHeight = series['TileHeight'];
        var countLevels = series['Resolutions'].length;

        // Maps always need a projection, but Zoomify layers are not geo-referenced, and
        // are only measured in pixels.  So, we create a fake projection that the map
        // can use to properly display the layer.
        var proj = new ol.proj.Projection({
          code: 'pixel',
          units: 'pixels',
          extent: [0, 0, width, height]
        });

        var extent = [0, -height, width, 0];

        // Disable the rotation of the map, and inertia while panning
        // http://stackoverflow.com/a/25682186

        var layer = new ol.layer.Tile({
          extent: extent,
          source: new ol.source.TileImage({
            projection: proj,
            tileUrlFunction: function (tileCoord, pixelRatio, projection) {
              return ('../tiles/' + seriesId + '/' +
                (countLevels - 1 - tileCoord[0]) + '/' + tileCoord[1] + '/' + (-tileCoord[2] - 1));
            },
            tileGrid: new ol.tilegrid.TileGrid({
              extent: extent,
              resolutions: series['Resolutions'].reverse(),
              tileSize: [tileWidth, tileHeight]
            })
          }),
          wrapX: false,
          projection: proj
        });

        var overviewMapControl = new ol.control.OverviewMap({
          // see in overviewmap-custom.html to see the custom CSS used
          className: 'ol-overviewmap ol-custom-overviewmap',
          layers: [layer],
          view: new ol.View({
            projection: proj
          }),
          collapseLabel: '\u00BB',
          label: '\u00AB',
          collapsed: false
        });

        var overviewMapControl2 = new ol.control.OverviewMap({
          view: new ol.View({
            projection: proj,
          })
        });

        var vectorLayer = new ol.layer.Vector({
          source: new ol.source.Vector()
        });

        var map = new ol.Map({
          controls: ol.control.defaults().extend([
            overviewMapControl
          ]),
          target: 'map',
          layers: [layer, vector],
          view: new ol.View({
            projection: proj,
            center: [width / 2, -height / 2],
            zoom: 0,
            minResolution: 1   // Do not interpelate over pixels
          })
        });
        //map.addControl(overviewMapControl);
        map.getView().fit(extent, map.getSize());

        //map.on('pointermove', pointerMoveHandler);

        /*map.getViewport().addEventListener('mouseout', function () {
          helpTooltipElement.classList.add('hidden');
        });*/

        var typeSelect = document.getElementById('type');
        var btnLineString = document.getElementById('lineString');
        var btnPolygon = document.getElementById('polygon');
        var btnDefault = document.getElementById('default');
        var draw; // global so we can remove it later
        
        /**
         * Format length output.
         * @param {module:ol.geom.LineString~LineString} line The line.
         * @return {string} The formatted length.
         */
        var formatLength = function (line) {
          console.log(line);
          var length = line.getLength();
          var output;
          if (length > 100) {
            output = (Math.round(length * 100 * spacingRow) / 100) +
              ' ' + 'um';
          } else {
            output = (Math.round(length * 100 * spacingRow) / 100) +
              ' ' + 'um';
          }
          return output;
        };


        /**
         * Format area output.
         * @param {module:ol.geom.Polygon~Polygon} polygon The polygon.
         * @return {string} Formatted area.
         */
        var formatArea = function (polygon) {
          var area = polygon.getArea();
          var output;
          if (area > 10000) {
            output = (Math.round(area * spacingRow * spacingRow * 100) / 100) +
              ' ' + 'um<sup>2</sup>';
          } else {
            output = (Math.round(area * spacingRow * spacingRow * 100) / 100) +
              ' ' + 'um<sup>2</sup>';
          }
          return output;
        };

        function addInteraction(type) {
          //var type = (typeSelect.value == 'area' ? 'Polygon' : 'LineString');
          if (type !== 'None') {
            draw = new ol.interaction.Draw({
              source: source,
              type: type,
              style: new ol.style.Style({
                fill: new ol.style.Fill({
                  color: 'rgba(255, 255, 255, 0.2)'
                }),
                stroke: new ol.style.Stroke({
                  color: 'rgba(0, 0, 0, 0.5)',
                  lineDash: [10, 10],
                  width: 2
                }),
                image: new ol.style.Circle({
                  radius: 5,
                  stroke: new ol.style.Stroke({
                    color: 'rgba(0, 0, 0, 0.7)'
                  }),
                  fill: new ol.style.Fill({
                    color: 'rgba(255, 255, 255, 0.2)'
                  })
                })
              })
            });
            map.addInteraction(draw);

            createMeasureTooltip();
            createHelpTooltip();

            var listener;
            draw.on('drawstart',
              function (evt) {
                // set sketch
                sketch = evt.feature;

                /** @type {module:ol/coordinate~Coordinate|undefined} */
                var tooltipCoord = evt.coordinate;

                listener = sketch.getGeometry().on('change', function (evt) {
                  var geom = evt.target;
                  var output;
                  if (geom instanceof ol.geom.Polygon) {
                    output = formatArea(geom);
                    tooltipCoord = geom.getInteriorPoint().getCoordinates();
                  } else if (geom instanceof ol.geom.LineString) {
                    output = formatLength(geom);
                    tooltipCoord = geom.getLastCoordinate();
                  }
                  measureTooltipElement.innerHTML = output;
                  measureTooltip.setPosition(tooltipCoord);
                });
              }, this);

            draw.on('drawend',
              function () {
                measureTooltipElement.className = 'tooltip tooltip-static';
                measureTooltip.setOffset([0, -7]);
                // unset sketch
                sketch = null;
                // unset tooltip so that a new one can be created
                measureTooltipElement = null;
                createMeasureTooltip();
                ol.Observable.unByKey(listener);
              }, this);
        }
        }


        /**
         * Creates a new help tooltip
         */
        function createHelpTooltip() {
          if (helpTooltipElement) {
            helpTooltipElement.parentNode.removeChild(helpTooltipElement);
          }
          helpTooltipElement = document.createElement('div');
          helpTooltipElement.className = 'tooltip hidden';
          helpTooltip = new ol.Overlay({
            element: helpTooltipElement,
            offset: [15, 0],
            positioning: 'center-left'
          });
          map.addOverlay(helpTooltip);
        }


        /**
         * Creates a new measure tooltip
         */
        function createMeasureTooltip() {
          if (measureTooltipElement) {
            measureTooltipElement.parentNode.removeChild(measureTooltipElement);
          }
          measureTooltipElement = document.createElement('div');
          measureTooltipElement.className = 'tooltip tooltip-measure';
          measureTooltip = new ol.Overlay({
            element: measureTooltipElement,
            offset: [0, -15],
            positioning: 'bottom-center'
          });
          map.addOverlay(measureTooltip);
        }


        /**
         * Let user change the geometry type.
         */
        /*typeSelect.onchange = function () {
          map.removeInteraction(draw);
          addInteraction();
        };*/
        btnLineString.onclick = function(){
          $("#lineString").addClass("active");
          $("#polygon").removeClass("active");
          $("#default").removeClass("active");
          map.removeInteraction(draw);
          addInteraction('LineString');
        };

        btnPolygon.onclick = function(){
          $("#lineString").removeClass("active");
          $("#polygon").addClass("active");
          $("#default").removeClass("active");
          map.removeInteraction(draw);
          addInteraction('Polygon');
        };

        btnDefault.onclick = function(){
          $("#lineString").removeClass("active");
          $("#polygon").removeClass("active");
          $("#default").addClass("active");
          map.removeInteraction(draw);
          addInteraction('None');
        };

        addInteraction('None');

      }
    });
  }
});
